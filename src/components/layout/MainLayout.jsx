import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import NavBar from './Navbar.jsx';
import Footer from './Footer.jsx';

class App extends Component {

  componentWillReceiveProps() {
    this.props.clearErrors();
  }

  render() {
    document.title = "Vigilant";
    return (
      <div className="site">
        <NavBar />
        <div className="siteContent">
          {this.props.children}
        </div>
        <Footer />
      </div>
    );
  }
}

export default connect(null, actions)(App);
