import React from 'react';

export default () => {
  return (
    <footer>
      <div className="container-fluid">

        <div className="copyright">
          &copy; Vigilant {new Date().getFullYear()}
        </div>
        <div className="slogan">
          Always Know.
        </div>

      </div>

    </footer>
  );
};
