import React, { Component, PropTypes } from 'react';
import {
  Navbar, Nav, NavItem,
  NavDropdown, MenuItem,
  Button, Input
} from 'react-bootstrap';
import { IndexLink, Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import * as actions from '../../actions/';

import SettingsDropdown from './SettingsDropdown.jsx';

class MainNavbar extends Component {
  constructor() {
    super();
    this.state = {
      routerListener: null,
      route: ''
    }
  }

  static contextTypes = {
    router: React.PropTypes.object
  }

  componentWillMount() {
    const { router } = this.context;
    const routerListener = router.listen(() => {
      this.setState({
        route: window.location.pathname
      });
    });

    this.setState({
      routerListener
    });
  }

  componentWillUnmount() {
    this.state.routerListener();
  }

  settingsActive() {
    return false;
  }

  searchLinkActive() {
    const router = this.context.router;
    if(router.isActive('/', true) || router.isActive('/search') || router.isActive('/results'))
      return "active";
  }

  linkActive(path) {
    if(this.context.router.isActive(path)) {
      return "active";
    }
  }

  render() {

    return (
      <Navbar staticTop>
        <div className="container">
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/search">Vigilant</Link>
            </Navbar.Brand>
          </Navbar.Header>

          <Navbar.Collapse>
            <Nav>
              <li className={this.searchLinkActive()}><Link to="/search"><i className="fa fa-search"/> Search </Link></li>
              <li className={this.linkActive('/history')}><Link to="/history"><i className="fa fa-archive"/> History </Link></li>
              <li className={this.linkActive('/alerts')}><Link to="/alerts"><i className="fa fa-bell-o"/> Alerts </Link></li>
              <li className={this.linkActive('/support')}><Link to="/support"><i className="fa fa-reply"/> Support </Link></li>
            </Nav>


            <Nav className='pull-right'>
              <SettingsDropdown active={this.settingsActive()}/>
              <li className='logout'><Link to="/logout"><i className="fa fa-power-off"/></Link></li>
            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>
    );
  }

};

export default connect(null, actions)(MainNavbar);
