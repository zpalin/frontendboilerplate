import React, { Component } from 'react';
import {
  Navbar, Nav, NavItem,
  NavDropdown, MenuItem,
  Button, Input
} from 'react-bootstrap';
import { IndexLink, Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import * as actions from '../../actions/';

class SettingsDropdown extends Component {
  renderDropdownTitle() {
    const { user: { firstName, lastName }} = this.props;
    const name = `${firstName} ${lastName}`;
    return <span><i className="fa fa-cogs settingsIcon"/> {name}</span>;
  }
  render() {
    const { user } = this.props;
    if(!user) return;

    return (
      <NavDropdown
        eventKey={3}
        title={this.renderDropdownTitle()}
        id="settingsDropdown"
        noCaret
      >
        <MenuItem onClick={() => browserHistory.push("/profile/edit")}>
          Edit Profile
        </MenuItem>
        <MenuItem onClick={() => browserHistory.push("/profile/password")}>
          Change Password
        </MenuItem>
        <MenuItem onClick={() => browserHistory.push("/profile/security")}>
          Security Settings
        </MenuItem>
        {
          user.admin
          ?
          [
            <MenuItem divider key="adminDivider"/>,
          <MenuItem key="manageUsersLink" onClick={() => browserHistory.push("/manage_users")}>
            Manage {user.organizationName} Users
          </MenuItem>,
          <MenuItem className={user.planType ? '' : 'hidden'} key="billingPage" onClick={() => browserHistory.push("/billing")}>
            Billing Settings
          </MenuItem>,

          ]
          : ''
        }
        {
          user.staff
          ?
          [
            <MenuItem divider key="staffDivider"/>,
          <MenuItem key="orgLink" onClick={() => browserHistory.push("/organizations")}>
            Manage Organizations
          </MenuItem>
          ]
          : ''
        }

        <MenuItem divider />
        <MenuItem onClick={() => {this.props.logoutUser()}}>Logout</MenuItem>
      </NavDropdown>
    );
  }
}
function bindState(state) {
  return {
    user: state.currentUser
  }
}

export default connect(bindState, actions)(SettingsDropdown);
