import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import $ from "jquery";
import _ from 'lodash';
import ES6Promise from 'es6-promise';

if(typeof Promise === 'undefined') {
  ES6Promise.polyfill();
}

window._ = _;
window.$ = window.jQuery = $;


import Router from './router';
import store from './store';
window.store = store;


function start() {
  ReactDOM.render(
    <Provider store={store}>
      <Router />
    </Provider>
    , document.querySelector('.react-mount'));
}
