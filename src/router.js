import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import store from './store';
import { SET_PREV_PATH } from './actions/types';

import RequireAuth from './components/auth/requireAuth.jsx';

import App from './components/App.jsx';

export default () => {
  return (
    <Router history={browserHistory}>
      <Route path='/' component={App}>
      </Route>
    </Router>
  );
}
