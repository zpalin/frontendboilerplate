let Settings = {
  // host: '192.168.99.100', debug: true,
 // host: 'localhost:3000', debug: true,
   // host: 'dev.vigilant.cc', debug: true,
  // host: '169.46.15.217',
  // host: 'docker.vigilant.cc', ssl: true, debug: true,
  host: 'vigilant.cc', ssl: true, debug: false,

  // host: '104.130.239.74',
  // host: '146.20.65.66',
}
Settings.protocol = Settings.ssl ? 'https' : 'http';
Settings.wsProtocol = Settings.ssl ? 'wss' : 'ws';
Settings.rootUrl = `${Settings.protocol}://${Settings.host}/api`

export default Settings;
